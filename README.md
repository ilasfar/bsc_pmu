Performance monitoring Unit - PMU

This repository contains the RTL and documentation for the unit. 


*  The specs for each feature and memory map calculator can be found under the ```docs``` folder.
*  Top levels for different configurations or wrappers are found in ```rtl```.
*  RTL for Submodules (MCCU, RDC, Counters, etc..) can be found in ```submodules```.
*  Synth contains scripts for early area and frequency evaluation with yosys.
*  ```tb``` contains testbenches and verification scripts.
